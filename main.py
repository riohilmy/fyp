import flask, flask.views
import compiled
import os, process
from werkzeug import secure_filename
from flask import Flask, jsonify

UPLOAD_FOLDER = 'D:\\FYP\\python-code\\uploads\\'
ALLOWED_EXTENSIONS = set(['txt'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#@app.route("/")
class Main(flask.views.MethodView):
    def get(self):
        return flask.render_template('index.html')

app.add_url_rule('/', view_func=Main.as_view('main'), methods=["GET"])

@app.route("/_analyze",  methods=['GET', 'POST'])
def analyze():

    file = flask.request.files['file']
    filename = secure_filename(file.filename)
    filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file.save(filepath)

    # @said: process video to text
    compiled.convert(filepath)
    compiled.transcribe()
    # pass processed-video to getScore method
    result = compiled.getScore("text1.txt")
    #result = process.getScore(filepath)

    #print(result)
    return jsonify(result)
app.debug = True

if __name__ == "__main__":
    app.run()