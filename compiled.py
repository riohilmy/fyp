from moviepy.video.io.VideoFileClip import VideoFileClip as mp
import os
import speech_recognition as sr

# obtain path to "english.wav" in the same folder as this script
from os import path

import process

GOOGLE_CLOUD_SPEECH_CREDENTIALS = r"""{
  "type": "service_account",
  "project_id": "i-trees-165313",
  "private_key_id": "962f7928b1afce11afe26512d20fae99c83d3738",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDVoeh1l/tzu5jn\nE/QlTcCllSO0JD8AcZ/tJnkJUz69LGjoBb9+GUhqsR/tHpQvMIFeOJL+yh84DuRL\nrnv17ilMcyysPDUPOdGmo3Ng/Ni1L8if6pbmOl0jCa6HEB39wSgnAei9L8KBVcl+\nbg91Fbq+h8bpyTDgP2RoZKe9R1T2ViJr7bs1RBDAPt4eSxaV4IFW8u1nNhu5q32R\naiMJKVJGg7ghVb2gkG/lQZ2P8ZmOjVZMe49HYaqVCmaPThDYh6nAQlHHQ4V7rF6m\n94q+F02HE5Us0HPZNqx25jLCBxfshKksrHXKXLrUlidvO/jMYRIa1GvSJVkH7mPn\niVKZeoidAgMBAAECggEBAIGGbAG4Ry3RRRB9qZ5gePgr53zWKRiky5kmpWYBAd4C\nqgxbR9I1lBGUGM5UpvXBi/5LgNxYQftkFAjK3r+qSYHfpus6idUUckd9fHwGcYHR\nWeuTeTBXnJaczSN4oyZyUVztLGZgKabvE37DTj3oEdRpUDH42NhttvwQYx9Au4Oq\ncvXFPoFcVvkL+uPlzAJjGWpaqBg9m1q/2baXybpsPcN0m7gcKJBHZ0XyZXukm16c\n4DtIRTu7mNFi3xTvghn69l2z6zdFKMIvbbNnEkkha8VzHLYIRlfi8hl+lhOM+4JY\nZfLmWpqnk/2gMLkIHPYOAeR6+Vk2J4lBQdq/1mJLYoUCgYEA/8nphI6VBg0ECtll\nyHIrKN79mmTHIIn/qF7iI84F8FmMpePa9ZrLMU1zPXIU5wTP55KcixEDki/vDYX5\n3HTepdsUpdZOxxLuaDlKHQ8pdRufA+5U/8qRCldHaOeHESzE3AC9QiTrzHxpsrRY\nWsrmac0PeGLxnEwTVKCcGrCsgXcCgYEA1c8U6uaNEuYvbbbCX6SgMg3slcg5zk+F\nxDYssHEsVi6+FagWZMA40PSia7NJYqkGKJVjsz9Fvq5CUQrVLI1F6WEaAK49WKAb\n9/GMOZExCWkmTPUHgIDhXrRWt4rjMrzP3HVLlilIdDWMrNnPAdI45bZZCyRrK/Uo\ng02kUzTI64sCgYALFpyp42hwZ7YKJlx0MQbP8cK6o63CKllL2jkhZjf5sFgZhOQk\nC37Nd1cMjdp8HUhJ+KASoz0tZCiPxIQNW3+fXvzpJ34owRhWqk0dL2ATtIvTJszG\nfeiFszzkPk0roG40P0/1Axa5uZb3aTv/S5Zrdbk69ccViHK6E3BQ8+G2hwKBgQDL\n1EVuwyhOkh85Cvg20ezeiyO2j1F8e5dcvQDRnLLV3m3cm4ftuMrxeaa14g/d/KEg\nY3rO0U0eQWZ7eEZN2MiDesydRX66VLkVILO71czX2eRnjVj/it5PuMDK7Z83KYVH\n+JJ8rxKhpsNbrbGJXO5dH/+3fZqmJ2vd8VDs+e9pjQKBgQDALzo/9E8ICunyJuYc\nnYYevgKJBxwazL3PSKRrEUfxqdUi9UkEwq5908AWqs3/1RDe7M3CI7nqRXTB5hiQ\nEdlVLQzQSeTMqCn0K8kD9ikpNsb0Rhd3axxy/R0mN+PIFW8nuY6ThsBktBJTdZSU\n54fU7jFXezkfaJwjDyllVCZxHw==\n-----END PRIVATE KEY-----\n",
  "client_email": "cloud-speech-api@i-trees-165313.iam.gserviceaccount.com",
  "client_id": "106710019400134756420",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://accounts.google.com/o/oauth2/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/cloud-speech-api%40i-trees-165313.iam.gserviceaccount.com"
}"""

def convert(filepath):

    clip = mp(filepath).subclip(0,60)
    clip.audio.write_audiofile("theaudio.wav")


def transcribe():
    AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "theaudio.wav")
    r = sr.Recognizer()####################### <----- error here
    fo = open("text1.txt", "w")
    with sr.AudioFile(AUDIO_FILE) as source:
        audio = r.record(source)  # read the entire audio file
    try:
        print("Google cloud started audio-to-text conversion..")
        result = r.recognize_google_cloud(audio, credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS)
        fo.write(result)
        fo.close()
        print("Audio-to-text conversion done.")

    except sr.UnknownValueError:
        return "Google Cloud Speech could not understand audio"
    except sr.RequestError as e:
        return "Could not request results from Google Cloud Speech service; {0}".format(e)

from senti_classifier import senti_classifier

def warmUp():
    senti_classifier.polarity_scores(["Keeping you warm.","Please stay warm."])

def getScore(file):
    return process.getScore(file)

