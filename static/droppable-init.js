// variable to store dragged files
var _files;

(function(window) {
	function triggerCallback(e, callback) {
		if (!callback || typeof callback !== 'function') {
			return;
		}
		var files;
		if (e.dataTransfer) {
			files = e.dataTransfer.files;
		} else if (e.target) {
			files = e.target.files;
		}
		callback.call(null, files);
	}

	function makeDroppable(ele, callback) {
		var input = document.createElement('input');
		input.setAttribute('type', 'file');
		input.setAttribute('multiple', true);
		input.style.display = 'none';
		input.addEventListener('change', function(e) {
			triggerCallback(e, callback);
		});
		ele.appendChild(input);
		ele.addEventListener('dragover', function(e) {
			e.preventDefault();
			e.stopPropagation();
			ele.classList.add('dragover');
		});
		ele.addEventListener('dragleave', function(e) {
			e.preventDefault();
			e.stopPropagation();
			ele.classList.remove('dragover');
		});
		ele.addEventListener('drop', function(e) {
			e.preventDefault();
			e.stopPropagation();
			ele.classList.remove('dragover');
			triggerCallback(e, callback);
		});
		ele.addEventListener('click', function() {
			input.value = null;
			input.click();
		});
	}
	window.makeDroppable = makeDroppable;
})(this);
(function(window) {
	makeDroppable(window.document.querySelector('.demo-droppable'), function(files) {
	    _files = files;
		var output = document.querySelector('.output');
		output.innerHTML = '';
		for (var i = 0; i < files.length; i++) {
			if (files[i].type.indexOf('image/') === 0) {
				output.innerHTML += '<center><img width="200" src="' + URL.createObjectURL(files[i]) + '"/></center>';
			}
			output.innerHTML += '<p><center>' + files[i].name + '</center></p>';
		}
	});
})(this);

function uploadFiles () {
// guard from empty upload file
if (!_files) {
        swal("Please upload the file first", "", "error");
        return false;
    }

// prompt user
swal({
  title: "Analyze video?",
  text: "Click submit to start the progress",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
},
function(){
      var fd = new FormData();
fd.append( 'file', _files[0] );

$.ajax({
  url: '/_analyze',
  data: fd,
  processData: false,
  contentType: false,
  type: 'POST',
      success: function(data){
        // sample result
//  [
//  [
//    "Positive score:",
//    "1.25",
//    "Negative score:",
//    "0.375"
//  ],
//  "\nConclusion: A positive review"
//]

        var result = data;
        var resultText = result.score + "\n" + result.conclusion;

        $('textarea#text-area').val(result.text);
        swal("Analyzing completed!", resultText, "success");
      },
      error: function (xhr) {
        swal("Something is wrong", "Analyzing failed: File format is not supported", "error");
      }
    });
});

    return true
}

$('#analyze').on('click', uploadFiles);