from havenondemand.hodclient import *
client = HODClient("88089162-b365-4eba-b1cf-0afd18db2bb2", "v1")

# params = {'file': 'path/to/file.mp3'} # if using a local file
params = {'url': 'https://www.havenondemand.com/sample-content/videos/hpnext.mp4'} # if using a publicly facing URL
response_async = client.post_request(params, HODApps.RECOGNIZE_SPEECH, async=True)
jobID = response_async['jobID']

def getJobAsyncJobStatus(jobID):
    print('Processing...')
    response = client.get_job_status(jobID)
    if response == None: # still transcribing...
        getJobAsyncJobStatus(jobID)
    else: # done trasncribing
        transcription = response['document'][0]['content']
        print(transcription)

getJobAsyncJobStatus(jobID)
