from senti_classifier import senti_classifier

def warmUp():
    senti_classifier.polarity_scores(["Keeping you warm.","Please stay warm."])

def getScore(file):
    print("getScore process started..")

    # TODO: fix TypeError: 'ImmutableMultiDict' object is not callable
    with open(file, 'r') as rfile:
        infile = rfile.read()

    text = infile
    pos_s, neg_s = senti_classifier.polarity_scores([text])

    score = "Positive score: " + str(pos_s) + "\nNegative score: " + str(neg_s)

    if pos_s > neg_s:
        conclusion = "Conclusion: A positive review"
    elif pos_s == neg_s:
        conclusion = "Conclusion: A neutral review"
    else:
        conclusion = "Conclusion: A Negative review"

    print("getScore process finished")
    return {"text":text, "score": score, "conclusion": conclusion}
# print(getScore('text1.txt'))